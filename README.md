# Vim + tmux configuration

Due to my frequent changing of my distro's I need some automatic workflow for 
installing my daily packages. This file thus contain my .vimrc and .tmux.conf 
for my daily workaround.

___

First install **git**, **curl**, **vim** and **tmux**. (If you using Neovim, 
you need to change some workaround - see below...)

Then put the following lines in your terminal: 

    git clone --depth=1 https://gitlab.com/ckue/vimrc.git ~/.vim_runtime
    sh ~/.vim_runtime/install_files.sh

Then go to **vim** and type in **:PlugInstall** for installing the required packages.


___

## If using Neovim
If you using Neovim
* than change in ~/.vim_runtime the files from \"junegunn\")