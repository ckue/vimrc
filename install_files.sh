#!/bin/sh
set -e

echo "Install vim-plug from \"junegunn\" "
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    
echo "Create working directory"
cd ~/.vim_runtime

cat ~/.vim_runtime/dir/.vimrc > ~/.vimrc
echo ".vimrc succesfully created"

cat ~/.vim_runtime/dir/.tmux.conf > ~/.tmux.conf
echo ".tmux.conf succesfully created"

echo "Remove working directory ~/.vim_runtime"
rm -rf ~/.vim_runtime
